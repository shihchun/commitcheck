﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;

namespace commitcheck
{

    public class CommitViewModel : INotifyPropertyChanged
    {
        private bool isCommits1Updated = false;
        private bool isCommits2Updated = false;
        private DataTable commitsCurrent;
        private DataTable commits1;
        private DataTable commits2;

        public DataTable CommitsCurrent
        {
            get { return commitsCurrent; }
            set
            {
                if (value != commitsCurrent)
                {
                    commitsCurrent = value;
                    CommitsCurrent.Columns.Add("IsMatchingSHA1", typeof(string)); // Blue
                    CommitsCurrent.Columns.Add("NotInCurrent", typeof(string)); // Red
                    OnPropertyChanged(nameof(CommitsCurrent));
                }
            }
        }

        public DataTable Commits1
        {
            get { return commits1; }
            set
            {
                Console.WriteLine("=====DataTable Commits1");
                if (value != null && value != commits1)
                {
                    commits1 = value;
                    Commits1.Columns.Add("IsMatchingSHA1", typeof(string)); // Blue/Yellow
                    Commits1.Columns.Add("NotInCurrent", typeof(string)); // Red

                    OnPropertyChanged(nameof(Commits1));

                    // 設置標誌，表示 Commits1 已經更新
                    isCommits1Updated = true;

                    // 檢查是否可以執行操作
                    TryExecuteOperation();
                }
            }
        }


        public DataTable Commits2
        {
            get { return commits2; }
            set
            {
                Console.WriteLine("=====DataTable Commits2");
                if (value != null && value != commits2)
                {
                    commits2 = value;
                    OnPropertyChanged(nameof(Commits2));

                    // 設置標誌，表示 Commits2 已經更新
                    isCommits2Updated = true;

                    // 檢查是否可以執行操作
                    TryExecuteOperation();
                }
            }
        }
        private void TryExecuteOperation()
        {
            // 檢查兩者是否都已經更新
            if (isCommits1Updated && isCommits2Updated)
            {
                // 執行你想要的操作
                UpdateDataGridColors();

                // 重置標誌，以便下次更新
                isCommits1Updated = false;
                isCommits2Updated = false;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void UpdateDataGridColors()
        {
            Console.WriteLine("=====UpdateDataGridColors");
            if (Commits1 == null || Commits2 == null)
                return;

            // Assuming SHA1 is in the "SHA1" column
            int sha1Index = CommitsCurrent.Columns.IndexOf("SHA1");

            // 1. Both DataGrids have the same SHA1, mark as blue
            MarkMatchingSHA1AsBlue(sha1Index);

        }

        private bool IsSHA1InDataTable(DataTable dataTable, string sha1)
        {
            Console.WriteLine("=====IsSHA1InDataTable");
            return dataTable.AsEnumerable().Any(row => row.Field<string>("SHA1") == sha1);
        }

        private void MarkMatchingSHA1AsBlue(int sha1Index)
        {
            Console.WriteLine("=====MarkMatchingSHA1AsBlue");
            foreach (DataRow row1 in Commits1.Rows)
            {
                //MessageBox.Show("Test", "歡迎", MessageBoxButton.OK, MessageBoxImage.Information);

                string sha1Value = row1.Field<string>("SHA1");

                bool isMatch = sha1Index != -1 && Commits2.AsEnumerable().Any(row2 => sha1Value.Equals(row2.Field<string>("SHA1")));
                bool NotInCurrent = sha1Index != -1 && CommitsCurrent.AsEnumerable().Any(row2 => sha1Value.Equals(row2.Field<string>("SHA1")));

                //Console.WriteLine($"SHA1: {sha1Value}, IsMatchingSHA1: {isMatch}");
                row1["IsMatchingSHA1"] = "false";
                row1["NotInCurrent"] = "false";

                row1["IsMatchingSHA1"] = isMatch.ToString();
                row1["NotInCurrent"] = (!NotInCurrent).ToString();
            }

            foreach (DataRow row1 in CommitsCurrent.Rows)
            {
                //MessageBox.Show("Test", "歡迎", MessageBoxButton.OK, MessageBoxImage.Information);

                string sha1Value = row1.Field<string>("SHA1");

                bool isMatch = sha1Index != -1 && Commits1.AsEnumerable().Any(row2 => sha1Value.Equals(row2.Field<string>("SHA1")));
                //bool NotInCurrent = sha1Index != -1 && Commits1.AsEnumerable().Any(row2 => sha1Value.Equals(row2.Field<string>("SHA1")));

                //Console.WriteLine($"SHA1: {sha1Value}, IsMatchingSHA1: {isMatch} NotInCurrent: {NotInCurrent}");
                row1["IsMatchingSHA1"] = "false";
                //row1["NotInCurrent"] = "false";

                row1["IsMatchingSHA1"] = isMatch.ToString();
                //row1["NotInCurrent"] = NotInCurrent.ToString();

            }

        }

    }
}
