﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data; // for datatable
using System.Diagnostics; // use cmd
using System.Collections.ObjectModel;
using ClosedXML.Excel;
using Microsoft.Win32;

namespace commitcheck
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        public Window2()
        {
            InitializeComponent();
            DataContext = new CommitViewModel(); // For INotifyPropertyChanged binding item
            System.Diagnostics.PresentationTraceSources.SetTraceLevel(myData1.ItemContainerGenerator, System.Diagnostics.PresentationTraceLevel.High);

        }

        private async Task WaitForExitAsync(Process process)
        {
            await Task.Run(() => process.WaitForExit());
        }
        public void SetTabNames(string pkgpath,string value1, string value2, string value3)
        {
            // 獲取MainWindow的傳數值來得到Commits Tables
            Tb1.Text = $"Current: {value1}";
            Tb2.Text = $"N-1: {value2}";
            Tb3.Text = $"N-2: {value3}";
            Dispatcher.Invoke(async () =>
            {
                try
                {
                    // 使用 Task.Run 將 GetCommits 部分移到非 UI 線程
                    Task<DataTable> task1 = Task.Run(() => GetCommits(pkgpath, value1));
                    Task<DataTable> task2 = Task.Run(() => GetCommits(pkgpath, value2));
                    Task<DataTable> task3 = Task.Run(() => GetCommits(pkgpath, value3));

                    // 使用 Task.WhenAll 等待所有異步操作完成
                    await Task.WhenAll(task1, task2, task3);

                    // 獲取結果
                    DataTable datatable1 = task1.Result.Copy();
                    DataTable datatable2 = task2.Result.Copy();
                    DataTable datatable3 = task3.Result.Copy();

                    // 在 UI 線程上更新 ViewModel
                    Dispatcher.Invoke(() =>
                    {
                        // 在 UI 線程上進行 UI 更新
                        updatemyData1(datatable1, datatable2, datatable3);
                    });
                }
                catch (Exception ex)
                {
                    // 處理異常
                    Console.WriteLine("=====GetCommits Exception");
                    Console.WriteLine($"Error: {ex.Message}");
                }
            });



        }

        private void updatemyData1(DataTable datatable1, DataTable datatable2, DataTable datatable3)
        {
            // MVVM(Model - View - ViewModel) filled by ViewModel
            var viewModel = (CommitViewModel)DataContext;

            // Use async/await to wait for the completion of the asynchronous operation
            // Set the CommitsCurrent property in the ViewModel
            viewModel.CommitsCurrent = datatable1;
            viewModel.Commits1 = datatable2;
            viewModel.Commits2 = datatable3;
            // Enable vertical and horizontal scroll bars
            myData1.SetValue(ScrollViewer.VerticalScrollBarVisibilityProperty, ScrollBarVisibility.Auto);
            myData1.SetValue(ScrollViewer.HorizontalScrollBarVisibilityProperty, ScrollBarVisibility.Auto);
        }

        private async Task<DataTable> GetCommits(string workingDirectory, string branchName)
        {
            //int i = 1;
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("Date");
            dataTable.Columns.Add("SHA1");
            dataTable.Columns.Add("Author");
            dataTable.Columns.Add("Email");
            dataTable.Columns.Add("Info");

            try
            {
                var psi = new ProcessStartInfo("git.exe", $"log --oneline -5000 --pretty=format:\" '%cs','%H','%an','%ae','%s'\" {branchName}")
                //var psi = new ProcessStartInfo("git.exe", $"log --oneline --pretty=format:\" '%cs','%H','%an','%ae','%s'\" {branchName}")
                {
                    RedirectStandardInput = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WorkingDirectory = workingDirectory
                };

                using (Process process = new Process { StartInfo = psi })
                {
                    process.OutputDataReceived += (s, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            //if (i == 1) { MessageBox.Show($"branch commits update: {branchName}" + Environment.NewLine + args.Data, "歡迎", MessageBoxButton.OK, MessageBoxImage.Information); i++; }
                            string[] parts = args.Data.Split(',');
                            //MessageBox.Show(args.Data, "歡迎", MessageBoxButton.OK, MessageBoxImage.Information);
                            //Assuming parts has 5 elements: date, sha1, author, email, info
                            if (parts.Length == 5)
                            {
                                DataRow row = dataTable.NewRow();
                                row["Date"] = parts[0].Trim(' ', '\'');
                                row["SHA1"] = parts[1].Trim(' ', '\'');
                                row["Author"] = parts[2].Trim(' ', '\'');
                                row["Email"] = parts[3].Trim(' ', '\'');
                                row["Info"] = parts[4].Trim(' ', '\'');
                                //MessageBox.Show(parts[4].Trim(' ', '\''), "歡迎", MessageBoxButton.OK, MessageBoxImage.Information);
                                dataTable.Rows.Add(row);
                                //dataTable.Rows.Add('');

                            }
                        }
                    };
                    process.ErrorDataReceived += (s, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            // Dispatcher.Invoke(() => outputTextBox.AppendText(args.Data + Environment.NewLine));
                            Dispatcher.Invoke(() =>
                            {
                                //outputTextBox.AppendText(args.Data + Environment.NewLine);
                                MessageBox.Show(args.Data, "歡迎", MessageBoxButton.OK, MessageBoxImage.Information);
                            });
                        }
                    };

                    process.Start();
                    process.BeginOutputReadLine();
                    //process.StandardInput.Close(); // Cause Error async execute no need to Closed will kill after function executed
                    await WaitForExitAsync(process);
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions
                Console.WriteLine("====Get Commit Error");
                Console.WriteLine($"Error: {ex.Message}");
            }

            // Now dataTable contains the commit information, and you can bind it to your DataGrid
            // Use Dispatcher to update UI from a non-UI thread
            //myData1.ItemsSource = dataTable.DefaultView;
            //MessageBox.Show("Filled", "歡迎", MessageBoxButton.OK, MessageBoxImage.Information);
            return dataTable;
        }

        private void MenuItem_Save(object sender, RoutedEventArgs e)
        {
            try
            {
                // 提示用戶選擇保存文件的位置
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel files (*.xlsx)|*.xlsx";
                saveFileDialog.Title = "Save Excel File";
                if (saveFileDialog.ShowDialog() == true)
                {
                    // 創建一個新的 Excel 工作簿
                    using (var workbook = new XLWorkbook())
                    {
                        var viewModel = (CommitViewModel)DataContext;
                        // 在工作簿中添加 Sheet，每個 Sheet 對應一個 DataTable
                        AddDataTableToSheet(workbook, Tb1.Text.Substring(Tb1.Text.LastIndexOf('/')).Replace('/', ' '), viewModel.CommitsCurrent);
                        AddDataTableToSheet(workbook, Tb2.Text.Substring(Tb2.Text.LastIndexOf('/')).Replace('/', ' '), viewModel.Commits1);
                        AddDataTableToSheet(workbook, Tb3.Text.Substring(Tb3.Text.LastIndexOf('/')).Replace('/', ' '), viewModel.Commits2);

                        // 將工作簿保存到文件
                        workbook.SaveAs(saveFileDialog.FileName);
                    }

                    Console.WriteLine("Excel file saved successfully.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
        }

        private void AddDataTableToSheet(XLWorkbook workbook, string sheetName, DataTable dataTable)
        {
            // 創建一個新的 Sheet
            var worksheet = workbook.Worksheets.Add(sheetName);

            // 寫入列名
            for (int col = 0; col < dataTable.Columns.Count; col++)
            {
                if (ShouldExportColumn(dataTable.Columns[col].ColumnName))
                {
                    worksheet.Cell(1, col + 1).Value = dataTable.Columns[col].ColumnName;
                }
            }

            // 寫入數據和應用顏色
            for (int row = 0; row < dataTable.Rows.Count; row++)
            {
                // 檢查列是否存在
                if (dataTable.Columns.Contains("IsMatchingSHA1") && dataTable.Columns.Contains("NotInCurrent"))
                {
                    bool isMatchingSHA1 = Convert.IsDBNull(dataTable.Rows[row]["IsMatchingSHA1"]) ? false : Convert.ToBoolean(dataTable.Rows[row]["IsMatchingSHA1"]);
                    bool notInCurrent = Convert.IsDBNull(dataTable.Rows[row]["NotInCurrent"]) ? false : Convert.ToBoolean(dataTable.Rows[row]["NotInCurrent"]);

                    // 根據條件更改整行的顏色
                    XLColor rowColor = GetRowColor(isMatchingSHA1, notInCurrent);

                    for (int col = 0; col < dataTable.Columns.Count; col++)
                    {
                        // 如果不需要導出的列，則跳過
                        if (!ShouldExportColumn(dataTable.Columns[col].ColumnName))
                        {
                            continue;
                        }

                        var cell = worksheet.Cell(row + 2, col + 1);
                        cell.Value = Convert.IsDBNull(dataTable.Rows[row][col]) ? null : Convert.ToString(dataTable.Rows[row][col]);

                        // 應用整行的顏色
                        cell.Style.Fill.BackgroundColor = rowColor;
                    }
                }
            }
        }


        private bool ShouldExportColumn(string columnName)
        {
            // 根據列名判斷是否導出該列
            // 這是一個示例，你可能需要根據具體的條件定義
            return columnName != "IsMatchingSHA1" && columnName != "NotInCurrent";
        }

        private XLColor GetRowColor(bool isMatchingSHA1, bool notInCurrent)
        {
            // 根據條件返回整行的顏色
            if (notInCurrent)
            {
                return XLColor.Red;
            }
            else if (isMatchingSHA1)
            {
                return XLColor.FromHtml("#FF00F3FF"); // Blue
                
            }
            else
            {
                return XLColor.NoColor; // 或者使用其他的顏色
            }
        }


    }
}
