﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IniParser; // nuget plugins: ini-parser for framework 4.7
using IniParser.Model;
using System.Data; // for datatable
using System.Diagnostics; // use cmd
using System.Collections.ObjectModel;
using System.ComponentModel;
// using System.Threading.Tasks; // thread
// dependency
// Install-Package ini-parser


namespace commitcheck
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        public MainWindow()
		{
			InitializeComponent();
            var parser = new FileIniDataParser();
			IniData data = parser.ReadFile("config.ini");
            try
            {
                string pkgPath = data["Paths"]["PkgPath"];
                string Checklist = data["Paths"]["checklist"];
                // 在主線程上執行異步事件
                outputTextBox.AppendText("[System] CheckList from ini file: " + Checklist + Environment.NewLine);
                outputTextBox.AppendText("[System] Git Package path load from ini file: " + pkgPath + Environment.NewLine);
                
                SetButtonsEnabled(false);
                //outputTextBox.AppendText("[System] Geting branch names..." + Environment.NewLine);
                Dispatcher.Invoke(async () => // no awit will run synchronously
                {
                    //await FetchBranchesAsync(@"git branch -a", pkgPath);
                    SetButtonsEnabled(true);
                });
                

            } catch (KeyNotFoundException){}

            // 將設定內容動態加入 DataTable 
            // commitDataGrid xml已經隱藏
            // string gitPath = data["Paths"]["BRANCH_NAME"];
            // MessageBox.Show(gitPath, "歡迎", MessageBoxButton.OK, MessageBoxImage.Information);
            DataTable dataTable = new DataTable();
			dataTable.Columns.Add("Section");
			dataTable.Columns.Add("Key");
			dataTable.Columns.Add("Value");

			// 將設定內容動態加入 DataTable
			foreach (var section in data.Sections)
			{
				foreach (var keyData in section.Keys)
				{
					var key = keyData.KeyName;
					var value = keyData.Value;
					dataTable.Rows.Add(section.SectionName, key, value);
				}
			}

			// 將 DataTable 資料綁定到 DataGrid
			commitDataGrid.ItemsSource = dataTable.DefaultView;

		}

        private async Task WaitForExitAsync(Process process)
        {
            await Task.Run(() => process.WaitForExit());
        }
        
        private async void Button_Click(object sender, RoutedEventArgs e) // git fetch button
		{
            SetButtonsEnabled(false);
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("config.ini");
            string pkgPath = data["Paths"]["PkgPath"];
            // 呼叫執行 CMD 指令的函數
            // await RunCmdCommandAsync(@"git branch -a | findstr ""remotes/origin"" | findstr ""BiosRelease""", pkgPath);
            // 使用 Task.Run 來執行非同步操作，以避免 UI 阻塞
            outputTextBox.AppendText("[System] Feching branch names..." + Environment.NewLine);
            await Task.Run(() => RunCmdCommandAsync("git fetch", pkgPath));
            //await FetchBranchesAsync(@"git branch -a | findstr ""remotes/origin/BiosRelease"" ", pkgPath);
            await FetchBranchesAsync(@"git branch -a | findstr ""remotes/origin/"" ", pkgPath);
            // 使用 Dispatcher.InvokeAsync 以確保在 UI 線程上執行
            SetButtonsEnabled(true);
        }

        private async void Button_Click2(object sender, RoutedEventArgs e) // git branch button
        {
            SetButtonsEnabled(false);
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("config.ini");
            string pkgPath = data["Paths"]["PkgPath"];
            // 呼叫執行 CMD 指令的函數
            outputTextBox.AppendText("[System] Geting branch names..." + Environment.NewLine);
            //await FetchBranchesAsync(@"git branch -a | findstr ""remotes/origin/BiosRelease"" ", pkgPath);
            await FetchBranchesAsync(@"git branch -a | findstr ""remotes/origin/"" ", pkgPath);

            // 使用 Dispatcher.InvokeAsync 以確保在 UI 線程上執行
            SetButtonsEnabled(true);
        }
        private async void Button_ShowCommits(object sender, RoutedEventArgs e) // git branch button
        {
            // FilteredComboBox
            // 取得 MainWindow 中的 ComboBox 數值
            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("config.ini");
            string pkgPath = data["Paths"]["PkgPath"];

            string comboBoxValue1 = FilteredComboBox1.SelectedItem?.ToString() ?? "(no value)";
            string comboBoxValue2 = FilteredComboBox2.SelectedItem?.ToString() ?? "(no value)";
            string comboBoxValue3 = FilteredComboBox3.SelectedItem?.ToString() ?? "(no value)";
            comboBoxValue1 = "  remotes/origin/BiosRelease/OptiHighLowMlkRpl/1.1.2";
            comboBoxValue2 = "  remotes/origin/BiosRelease/OptiHighLowMlkRpl/1.1.1";
            comboBoxValue3 = "  remotes/origin/BiosRelease/OptiHighLowMlkRpl/1.1.0";
            if (comboBoxValue1 == "(no value)") { MessageBox.Show("Current not insert", "歡迎", MessageBoxButton.OK, MessageBoxImage.Information); }
            else if (comboBoxValue2 == "(no value)") { MessageBox.Show("N-1 not insert", "歡迎", MessageBoxButton.OK, MessageBoxImage.Information); }
            else if (comboBoxValue3 == "(no value)") { MessageBox.Show("N-2 not insert", "歡迎", MessageBoxButton.OK, MessageBoxImage.Information); }
            else
            {
                //MessageBox.Show(gitPath, "歡迎", MessageBoxButton.OK, MessageBoxImage.Information);
                Window2 commitsWindow = new Window2();
                commitsWindow.SetTabNames(pkgPath, comboBoxValue1, comboBoxValue2, comboBoxValue3);
                commitsWindow.Show();
            }
            
        }

        private async Task RunCmdCommandAsync(string command, string workingDirectory) // Asyc cmd 不return
        {

            var psi = new ProcessStartInfo("cmd.exe")
            {
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                WorkingDirectory = workingDirectory
            };

            using (Process process = new Process { StartInfo = psi })
            {
                process.OutputDataReceived += (s, args) =>
                {
                    if (!string.IsNullOrEmpty(args.Data))
                    {
                        Dispatcher.Invoke(() =>
                        {
                            outputTextBox.AppendText(args.Data + Environment.NewLine);
                            outputTextBox.ScrollToEnd();
                        });
                    }
                };

                process.ErrorDataReceived += (s, args) =>
                {
                    if (!string.IsNullOrEmpty(args.Data))
                    {
                        // Dispatcher.Invoke(() => outputTextBox.AppendText(args.Data + Environment.NewLine));
                        Dispatcher.Invoke(() =>
                        {
                            outputTextBox.AppendText(args.Data + Environment.NewLine);
                            outputTextBox.ScrollToEnd();
                        });
                    }
                };

                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.StandardInput.WriteLine(command);
                process.StandardInput.Close();
                await WaitForExitAsync(process);
            }
        }
        
        private void SetButtonsEnabled(bool isEnabled) // 設定button屬性
        {
            // 在這裡，你需要將 btn1、btn2 替換為實際的按鈕名稱
            btn1.IsEnabled = isEnabled;
            btn2.IsEnabled = isEnabled;
            btn3.IsEnabled = isEnabled;
            btn4.IsEnabled = isEnabled;
            // 如果有其他按鈕，也繼續添加它們的啟用狀態設定
        }

        private async Task FetchBranchesAsync(string command, string workingDirectory)
        {
            List<string> outputList = new List<string>();
            // 將 List<string> 轉換成 ObservableCollection<string>
            try
            {
                var psi = new ProcessStartInfo("cmd.exe")
                {
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WorkingDirectory = workingDirectory
                };

                using (Process process = new Process { StartInfo = psi })
                {

                    process.OutputDataReceived += (s, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            Dispatcher.Invoke(() =>
                            {
                                if (args.Data.Contains("remotes/origin") && !args.Data.Contains("git branch"))
                                {
                                    outputList.Add(args.Data);
                                    //outputTextBox.AppendText(args.Data + Environment.NewLine);
                                    //outputTextBox.ScrollToEnd();
                                }
                            });
                        }
                    };

                    process.ErrorDataReceived += (s, args) =>
                    {
                        if (!string.IsNullOrEmpty(args.Data))
                        {
                            Dispatcher.Invoke(() =>
                            {
                                if (args.Data.Contains("remotes/origin") && !args.Data.Contains("git branch"))
                                {
                                    outputList.Add(args.Data);
                                    //outputTextBox.AppendText(args.Data + Environment.NewLine);
                                    //outputTextBox.ScrollToEnd();
                                }
                            });
                        }
                    };

                    process.Start();
                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();
                    process.StandardInput.WriteLine(command);
                    process.StandardInput.Close();

                    await WaitForExitAsync(process);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
            }


            List<String> names = new List<string>();

            // 更新 BranchOptions 並通知界面更新
            //branchComboBox.Items.Clear();
            //FilteredComboBox1.Items.Clear();
            FilteredComboBox1.ItemsSource = null;
            FilteredComboBox1.IsEditable = true;
            FilteredComboBox1.IsTextSearchEnabled = false;
            FilteredComboBox1.ItemsSource = outputList;

            FilteredComboBox2.ItemsSource = null;
            FilteredComboBox2.IsEditable = true;
            FilteredComboBox2.IsTextSearchEnabled = false;
            FilteredComboBox2.ItemsSource = outputList;

            FilteredComboBox3.ItemsSource = null;
            FilteredComboBox3.IsEditable = true;
            FilteredComboBox3.IsTextSearchEnabled = false;
            FilteredComboBox3.ItemsSource = outputList;
            //foreach (string i in outputList)
            //{
            //    outputTextBox.AppendText(i + Environment.NewLine);
            //}
            outputTextBox.ScrollToEnd();
            outputTextBox.AppendText("[System] Branch name get end" + Environment.NewLine);
        }

    }
}
