﻿using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace commitcheck
{
    public class FilteredComboBox : ComboBox
    {
        public static readonly DependencyProperty MinimumSearchLengthProperty =
            DependencyProperty.Register(
                "MinimumSearchLength",
                typeof(int),
                typeof(FilteredComboBox),
                new UIPropertyMetadata(3));

        private string oldFilter = string.Empty;
        private string currentFilter = string.Empty;

        public FilteredComboBox()
        {
        }

        public int MinimumSearchLength
        {
            get { return (int)GetValue(MinimumSearchLengthProperty); }
            set { SetValue(MinimumSearchLengthProperty, value); }
        }

        protected TextBox EditableTextBox
        {
            get { return GetTemplateChild("PART_EditableTextBox") as TextBox; }
        }

        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            if (newValue != null)
            {
                ICollectionView view = CollectionViewSource.GetDefaultView(newValue);
                view.Filter += FilterPredicate;
            }

            if (oldValue != null)
            {
                ICollectionView view = CollectionViewSource.GetDefaultView(oldValue);
                view.Filter -= FilterPredicate;
            }

            base.OnItemsSourceChanged(oldValue, newValue);
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                IsDropDownOpen = false;
            }
            else if (e.Key == Key.Escape)
            {
                IsDropDownOpen = false;
                SelectedIndex = -1;
                Text = currentFilter;
            }
            else
            {
                if (e.Key == Key.Down)
                {
                    IsDropDownOpen = true;
                }

                base.OnPreviewKeyDown(e);
            }

            oldFilter = Text;
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                // Navigation keys are ignored
            }
            else if (e.Key == Key.Tab || e.Key == Key.Enter)
            {
                ClearFilter();
            }
            else
            {
                if (Text != oldFilter)
                {
                    if (Text.Length == 0 || Text.Length >= MinimumSearchLength)
                    {
                        RefreshFilter();
                        IsDropDownOpen = true;
                        EditableTextBox.SelectionStart = int.MaxValue;
                    }
                }

                base.OnKeyUp(e);
                currentFilter = Text;
            }
        }

        protected override void OnPreviewLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            ClearFilter();
            int temp = SelectedIndex;
            SelectedIndex = -1;
            Text = string.Empty;
            SelectedIndex = temp;
            base.OnPreviewLostKeyboardFocus(e);
        }

        private void RefreshFilter()
        {
            if (ItemsSource != null)
            {
                ICollectionView view = CollectionViewSource.GetDefaultView(ItemsSource);
                view.Refresh();
            }
        }

        private void ClearFilter()
        {
            currentFilter = string.Empty;
            RefreshFilter();
        }

        private bool FilterPredicate(object value)
        {
            if (value == null)
            {
                return false;
            }

            if (Text.Length == 0)
            {
                return true;
            }

            return value.ToString().ToLower().Contains(Text.ToLower());
        }
    }
}
