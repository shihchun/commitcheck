# commitcheck

Tools for check commit, Implement by WPF .net 4.7

![](./imgs/Snipaste_2024-01-19_10-38-55.jpg)

![](./imgs/Snipaste_2024-01-19_10-39-27.jpg)

![](./imgs/Snipaste_2024-01-19_10-40-01.jpg)

![](./imgs/Snipaste_2024-01-19_10-40-31.jpg)

![](./imgs/Snipaste_2024-01-19_10-40-42.jpg)


## NGet dependency

```sh
Install-Package ClosedXML
Install-Package ini-parser
```

```csharp
// dependency
// Install-Package ini-parser
using IniParser; // nuget plugins: ini-parser for framework 4.7
```